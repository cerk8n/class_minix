# With VNC
qemu-system-i386 -drive file=minix3.1.0-book-version-harddisk.img,format=raw -boot order=c

# Without VNC
# qemu-system-i386 -drive file=minix3.1.0-book-version-harddisk.img,format=raw -boot order=c
