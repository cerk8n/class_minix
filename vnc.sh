#!/bin/bash

# ./qemuminix3.sh launches (on fedora defaults to vnc)
# With sdel turned off, or `-vnc :0` turned on,
# this enbles vnc'ing in with another client,
# allowing better visibility

# tigervnc
# vncviewer localhost:5900 

# vinagre
vinagre localhost:5900
# Can scale mode for better visibility
# Escape keyboard and mouse capture with F10
