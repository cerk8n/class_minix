#!/bin/bash

if [[ $# -ne 1 ]]
then
    echo "$0:"
    echo "Pass the directory to be processed by cflow"
    exit 1
fi

cd "$1"/
find . -iname "*.c" >./cflow.files
find . -iname "*.h" >>./cflow.files
# display
# cflow -f dot $(cat cflow.files) | dot -Txlib
# To png
cflow -f dot $(cat cflow.files) | dot -Tpng -o cflow.png
cflow $(cat cflow.files) -o cflow.out
rm cflow.files
