#!/bin/bash

find . -iname *.h -o -iname *.c | xargs clang-format -i --sort-includes=false
find . -iname *.h -o -iname *.cpp | xargs clang-format -i --sort-includes=false
