# Classroom Minix3
This version of Minix3 matches lectures and the latest version of the Minix textbook.

Unpacking Minix:
- The first time, unzip the disk image, via `$ unzip *.zip`.

Running Minix:

- Start the system with `$ ./qemuminix3.sh`.
- Optionally, connect to it with a remote graphical client with: `$ ./vnc.sh`.
  This one is Linux only (feel free to choose another).
  Your system may launch qemu with a visual interface,
  but you can alternatively use VNC to connect:
  https://en.wikipedia.org/wiki/Virtual_Network_Computing
- In Minix, Login with username: `root` and no password.
- Use Minix.
- When you are done, Exit MINIX with CTRL+ALT+DEL.
  (if you do not, you may lose files that were not synced to the hard disk).
  You'll then need to close QEMU manually (MINIX doesn't know how).

If you want to edit Minix:

- Mount the `root`, `usr`, and `home` partitions from the image file to your host, with `$ ./mountminix3.sh`.
  Linux requires superuser permissions to mount a file system.
- Make your edits to the code (in your host) in `minix3bookmnt/`
- Unmount the filesystems using `$ ./umountminix3.sh` (also requires sudo).
- Start the system with `$ ./qemuminix3.sh`
- In Minix itself, navigate to the directory in which you are making edits, and recompile using the corresponding Makefile.
- You can also make edits directly in Minix, without needing to mount/umount.

If you break Minix:

- If you corrupt the image, or mess up your install, 
  then just delete the image, `$ rm *.img`,
  and start back over at the beginning.
